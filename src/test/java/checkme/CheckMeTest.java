package checkme;

import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class CheckMeTest {

    protected WebDriver driver;
    protected String url;
    protected WebDriverWait wait;

    protected Connection conn = null;

    //Данные для добавления в таблицу
    protected List<String[]> dataForFill = new ArrayList<String[]>() {{
        add(new String[]{"Товар", "12", "1223"});
        add(new String[]{"Товар1", "123", "45"});
        add(new String[]{"Товар2", "13", "457"});
    }};;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver83");

        url = "https://checkme.kavichki.com/";

        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 30);

        conn = getConn();
    }

    @After
    public void tearDown() {
        driver.quit();
        closeConn();
    }

    protected void migrateDatabase() {
        try {
            Properties props = new Properties();
            props.load(new FileInputStream(new File("src/config/config.ini")));

            Class.forName("com.mysql.jdbc.Driver");
            String userName = props.getProperty("DB_USER");
            String password = props.getProperty("DB_PASS");
            String url = props.getProperty("DB_URL");

            Flyway flyway = Flyway.configure().dataSource(url, userName, password).load();
            flyway.clean();
            flyway.migrate();
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        }
    }

    //Подключение к БД
    protected Connection getConn() {
        if (conn == null) {
            try {

                Properties props = new Properties();
                props.load(new FileInputStream(new File("src/config/config.ini")));

                Class.forName("com.mysql.jdbc.Driver");
                String userName = props.getProperty("DB_USER");
                String password = props.getProperty("DB_PASS");
                String url = props.getProperty("DB_URL");

                Properties properties = new Properties();
                properties.setProperty("user", userName);
                properties.setProperty("password", password);
                properties.setProperty("characterEncoding", "UTF-8");

                conn = DriverManager.getConnection(url, properties);

            } catch (Exception ex) {
                System.err.println("Cannot connect to database server");
                ex.printStackTrace();
            }
        }
        return conn;
    }

    //Завершение соединения с БД
    protected void closeConn() {
        if (conn != null) {
            try {

                conn.close();
                // System.out.println ("\n\nDatabase connection terminated...");
            } catch (Exception ex) {
                System.out.println("Error in connection termination!");
            }
        }
    }

    //Открытие страницы
    protected void openPage() {
        driver.get(url);
    }

    //Чтение данных из БД
    protected List<String[]> readDb() {
        List<String[]> records=new ArrayList<String[]>();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT name, quantity, price FROM purchases ");

            while(rs.next()){
                String[] row = new String[255];
                row[0] = rs.getString("name");
                row[1] = rs.getString("quantity");
                row[2] = rs.getString("price");

                records.add(row);
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) { }

                stmt = null;
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) { }

                rs = null;
            }
        }

        return records;
    }

    //Запись данных в БД
    protected boolean insertDb(String name, String quantity, String price) {
        Boolean res = false;
        PreparedStatement preparedStmt = null;
        try {
            String query = " insert into purchases (name, quantity, price)"
                    + " values (?, ?, ?)";

            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, name);
            preparedStmt.setString(2, quantity);
            preparedStmt.setString(3, price);

            res = preparedStmt.execute();
        } catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        finally {
            if (preparedStmt != null) {
                try {
                    preparedStmt.close();
                } catch (SQLException sqlEx) { } // ignore
                preparedStmt = null;
            }
        }

        return res;
    }

    //Получение данных из таблицы на странице
    protected List<String[]> parsePage() {
        List<String[]> records=new ArrayList<String[]>();
        String name ="";
        String quantity = "";
        String price = "";


        wait.withMessage("Не найдена таблица с данными на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("tbl")));

        WebElement tbl = driver.findElement(By.id("tbl"));
        WebElement tbody = tbl.findElement(By.cssSelector("tbody"));
        List<WebElement> trs = tbody.findElements(By.cssSelector("tr"));

        for (WebElement tr: trs) {
            name = tr.findElement(By.cssSelector("td:nth-child(1)")).getText();
            quantity = tr.findElement(By.cssSelector("td:nth-child(2)")).getText();
            price = tr.findElement(By.cssSelector("td:nth-child(3)")).getText();

            String[] row = new String[255];
            row[0] = name;
            row[1] = quantity;
            row[2] = price;

            records.add(row);
        }

        return records;
    }

    //Добавление данных в таблицу на странице
    protected void fillTable() {

        wait.withMessage("Не найдена кнопка добавления записи в таблицу на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("open")));

        WebElement buttonAdd = driver.findElement(By.id("open"));
        buttonAdd.click();
        wait.withMessage("Не найдены поля для заполнения в таблицу на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("add")));
        WebElement inputName = driver.findElement(By.id("name"));
        WebElement inputCount = driver.findElement(By.id("count"));
        WebElement inputPrice = driver.findElement(By.id("price"));

        for(String[] r: dataForFill) {
            inputName.clear();
            inputName.sendKeys(r[0]);
            inputCount.clear();
            inputCount.sendKeys(r[1]);
            inputPrice.clear();
            inputPrice.sendKeys(r[2]);
            driver.findElement(By.id("add")).click();
        }
    }

    //Проверка данных, полученных из БД и со страницы
    @Test
    public void DataTest() {
        List<String[]> records;
        List<String[]> recordsRes;
        List<String[]> recordsDb;
        Boolean flagForBdPage  = true;
        Boolean flagForPageEntered  = true;

        migrateDatabase();

        openPage();

        records = parsePage();

        for(String[] r: records) {
            insertDb(r[0], r[1], r[2]);
        }
        fillTable();

        recordsDb = readDb();
        recordsRes = parsePage();

        System.out.println ("\n-------------Сравнение строк в БД и на странице-------------\n");
        for (String[] rowRes : recordsRes) {
            boolean flag = false;
            for (String[] r: recordsDb) {

                if ((rowRes[0].equals(r[0])) && (rowRes[1].equals(r[1])) && (rowRes[2].equals(r[2]))){
                    System.out.println("Строка есть в базе: " + rowRes[0]+" - "+ rowRes[1]+" - "+ rowRes[2]);
                    flag = true;
                }

            }
            if (!flag) {
                System.out.println("Строки нет в базе: " + rowRes[0]+" - "+ rowRes[1]+" - "+ rowRes[2]);
                flagForBdPage = false;
            }
        }

        System.out.println ("\n-------------Сравнение строк на странице и введенных данных-------------\n");
        for (String[] rowRes : recordsRes) {
            for (String[] r: dataForFill) {

                if ((rowRes[0].equals(r[0]))){
                    if ((!rowRes[1].equals(r[1]) || !rowRes[2].equals(r[2]))) {
                        System.out.println("Строка отличается: " + rowRes[0] + " - " + rowRes[1] + " - " + rowRes[2]);
                        flagForPageEntered = false;
                    } else {
                        System.out.println("Строка совпадает: " + rowRes[0] + " - " + rowRes[1] + " - " + rowRes[2]);
                    }
                }

            }
        }
        Assert.assertTrue("Данные в БД и на странице отличаются ", flagForBdPage);
        Assert.assertTrue("Введенные данные и данные на странице отличаются ", flagForPageEntered);
    }

    //Проверка правильно ли задан заголовок страницы
    @Test
    public void PageTitleTest() {
        String expected = "Список покупок";

        openPage();

        String value = driver.getTitle();

        Assert.assertEquals("Неверный заголовок страницы ", expected, value);
    }

    //Проверка правильно ли отрабатывает кнопка Добавить новое
    @Test
    public void EditFormDisplayedTest() {

        openPage();

        wait.withMessage("Не найдена кнопка добавления записи в таблицу на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("open")));

        WebElement buttonAdd = driver.findElement(By.id("open"));
        buttonAdd.click();
        wait.withMessage("Отсутствует кнопка добавления товара на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("add")));
        Assert.assertTrue("Нет поля для заполнения наименования товара ", driver.findElement(By.id("name")).isDisplayed());
        Assert.assertTrue("Нет поля для заполнения количества ", driver.findElement(By.id("count")).isDisplayed());
        Assert.assertTrue("Нет поля для заполнения стоимости ", driver.findElement(By.id("price")).isDisplayed());
        Assert.assertTrue("Отсутствует кнопка добавления товара ", driver.findElement(By.id("add")).isDisplayed());
    }

    //Проверка правильно ли отрабатывает кнопка Сбросить
    @Test
    public void ButtonClearTest() {

        openPage();

        wait.withMessage("Не найдена кнопка добавления записи в таблицу на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("open")));
        WebElement buttonAdd = driver.findElement(By.id("open"));
        buttonAdd.click();
        WebElement inputName = driver.findElement(By.id("name"));
        WebElement inputCount = driver.findElement(By.id("count"));
        WebElement inputPrice = driver.findElement(By.id("price"));

        inputName.clear();
        inputName.sendKeys("Товар");
        inputCount.clear();
        inputCount.sendKeys("111");
        inputPrice.clear();
        inputPrice.sendKeys("222");

        buttonAdd.findElement(By.xpath("./following::a")).click();

        Assert.assertEquals("Поле для ввода наименования товара не очищено ", "", inputName.getAttribute("value"));
        Assert.assertEquals("Поле для ввода количества товара не очищено ", "", inputCount.getAttribute("value"));
        Assert.assertEquals("Поле для ввода стоимости товара не очищено ", "", inputPrice.getAttribute("value"));
    }

    //Проверка правильно ли отрабатывает кнопка Добавить
    @Test
    public void ButtonAddTest() {
        List<String[]> recordsRes;
        String ValueName = "Товар";
        String ValueCount = "12";
        String ValuePrice = "34";
        String ValueCountFromTable = "";
        String ValuePriceFromTable = "";
        openPage();

        wait.withMessage("Не найдена кнопка добавления записи в таблицу на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("open")));

        WebElement buttonAdd = driver.findElement(By.id("open"));
        buttonAdd.click();
        wait.withMessage("Не найдены поля для заполнения в таблицу на странице");
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("add")));

        driver.findElement(By.id("name")).sendKeys(ValueName);
        driver.findElement(By.id("count")).sendKeys(ValueCount);

        driver.findElement(By.id("price")).sendKeys(ValuePrice);
        driver.findElement(By.id("add")).click();

        recordsRes = parsePage();

        boolean flag = false;

        for (String[] rowRes : recordsRes) {
                if (rowRes[0].equals(rowRes[0])){
                    flag = true;
                    ValueCountFromTable = rowRes[1];
                    ValuePriceFromTable = rowRes[2];
                }
        }

        Assert.assertEquals("Товар не добавлен в таблицу ", true, flag);
        Assert.assertEquals("Введенное количество не совпадает с полученным ", ValueCount, ValueCountFromTable);
        Assert.assertEquals("Введенная стоимость не совпадает с полученной ", ValuePrice, ValuePriceFromTable);
    }
}
