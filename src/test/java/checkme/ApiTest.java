package checkme;

import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;

public class ApiTest {

    //Get a resource
    @Test
    public void apiGetAResourceTest() {
        RestAssured.registerParser("text/plain", Parser.JSON);

        Response response1 =
                RestAssured.given()
                        .log().all() //запиcываем в консоль
                        .when()
                        .get("https://jsonplaceholder.typicode.com/posts/1") //отправляем запрос get
                        .then()
                        .log().all() //запиcываем в консоль
                        .statusCode(200)  //сервер вернул код ответа с успешным статусом 200
                        .body("id", Matchers.equalTo(1)) //проверяем, что вернулся результат для id = 1
                        .extract()
                        .response();
        response1.getBody().print(); //печатаем ответ
    }

    //Create a resource
    @Test
    public void apiCreateAResourceTest() {
        RestAssured.registerParser("text/plain", Parser.JSON);

        Map<String, Object> jsonAsMap  = new HashMap<>();
        jsonAsMap .put("title", "foo");
        jsonAsMap .put("body", "bar");
        jsonAsMap .put("userId", 1);

        Gson gson = new Gson();
        String json = gson.toJson(jsonAsMap);

        Response response1 =
                RestAssured.given()
                        .log().all() //запиcываем в консоль
                        .with()
                        .header(new Header("Content-type", "application/json; charset=UTF-8"))
                        .body(json)
                        .when()
                        .post("https://jsonplaceholder.typicode.com/posts") //отправляем запрос post
                        .then()
                        .log().all() //запиcываем в консоль
                        .statusCode(201)  //сервер вернул код ответа с успешным статусом 201
                        .body("title", Matchers.equalTo("foo")) //проверяем, что был добавлен объект с title = "foo"
                        .body("body", Matchers.equalTo("bar")) //проверяем, что был добавлен объект с body = "bar"
                        .body("userId", Matchers.equalTo(1)) //проверяем, что был добавлен объект с userId = 1
                        .extract()
                        .response();
        response1.getBody().print(); //печатаем ответ
    }


}
